;;; -*- no-byte-compile: t -*-
(define-package "ob-racket" "1.2.0" "Org Babel Support for Racket."
  '((org "9.5")))
